//
// Created by bart on 31-12-18.
//

#include "Engine/src/epzh.h"
#include "Engine/src/Game/Drawable/Circle.h"
#include "Engine/src/Game/Drawable/Cross.h"
#include "Engine/src/Engine/Window.h"
#include "Engine/src/Engine/Draw.h"
#include "Engine/src/Engine/GameObject.h"
#include "Engine/src/Game/Drawable/Tile.h"
#include "Engine/src/Engine/UI/Button.h"
#include "Engine/src/Game/Grid.h"
#include "Engine/src/Game/Scenes/StartScene.h"
#include "Engine/src/Game/GameSceneManager.h"

int main() {
    Engine::Log::Init();
    EN_INFO("Starting");

    Engine::GameObject g;
    Game::GameSceneManager& coordinator = Game::GameSceneManager::getInstance();
    coordinator.registerAllScenes();

    g.init();
    Engine::GameObject::GLOBAL_DEBUG = false;

    g.createWindow(800, 800);

    g.start();
    while(g.isRunning()) {
        g.run();
    }
    return 0;
}