# Rows

Circles and squares game

Goal of this project is to experiment with the game library Allegro and with building games. 


## Building
Dependencies:
* Allegro 5 installed
   * `sudo add-apt-repository ppa:allegro/5.2`
   * `sudo apt-get update`
   * `sudo apt-get install liballegro5-dev`
* Boost library