//
// Created by bart on 24-1-19.
//

#ifndef ROWS_GRID_H
#define ROWS_GRID_H

#include "../epzh.h"
#include "../Engine/Types/Position.h"
#include "../Engine/Types/Dimension.h"
#include "Grid_Element.h"

namespace Game {
class Grid : public Engine::Base_Element, public Engine::Listener {
        int num_horizontal = 3;
        int num_vertical = 3;
        Engine::Dimension element_size;
        bool drawCircle = true;
        std::vector<Game::Grid_Element *> grid;

        std::array<int, 8> draw_scores = {0,0,0,0,0,0,0,0};

        int winner = 0;
        std::function<void()> void_callback;


    void draw_score_line(const Engine::Position &start, const Engine::Position &end);
public:
        bool hasWinner();
        int getWinner();
    void setVoidCallback(std::function<void()>);

    Grid(const Engine::Position &pos, const Engine::Dimension &dim, const Engine::Dimension &grid_element_dim);
        Grid(int x, int y, int w, int h, int gridWidth, int gridHeight);

        void replaceElement(int x, int y, Grid_Element * newElement);

        void addElement(int x, int y, Grid_Element * element);

        void draw() override;

        bool hasPosition(Engine::Position p) override;

        Base_Element *getNestedPosition(Engine::Position p) override;

        void checkScore();

        void drawScore();

    void sendUpdate(Engine::Event * event) override;
};
}

#endif //ROWS_GRID_H
