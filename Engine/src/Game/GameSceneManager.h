//
// Created by bacox on 22-5-19.
//

#ifndef ROWS_GAMESCENEMANAGER_H
#define ROWS_GAMESCENEMANAGER_H

#include "../Engine/SceneCoordinator.h"

namespace Game {
    class GameSceneManager : public Engine::SceneCoordinator{
    private:
        // private constructor, because of singleton structure
        GameSceneManager();
    public:
        static GameSceneManager &getInstance();

        GameSceneManager(GameSceneManager const &) = delete;

        void operator=(GameSceneManager const &) = delete;

        void registerAllScenes() override;
    };
}


#endif //ROWS_GAMESCENEMANAGER_H
