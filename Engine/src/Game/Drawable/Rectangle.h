//
// Created by bacox on 24-5-19.
//

#ifndef ROWS_RECTANGLE_H
#define ROWS_RECTANGLE_H

#include <color.h>
#include "../../Engine/Types/Position.h"
#include "../../Engine/Types/Dimension.h"
#include "../../Engine/Base_Element.h"

namespace Game {
    namespace Drawable {
    class Rectangle : public Engine::Base_Element{
    public:
        Rectangle(int x, int y, int w, int h);
        Rectangle(const Engine::Position &pos, const Engine::Dimension &dim);

        void setBackgroundColor(const ALLEGRO_COLOR &c);

        void setBorderColor(const ALLEGRO_COLOR &c);

        void draw() override;

    private:
            ALLEGRO_COLOR backgroundColor;
            ALLEGRO_COLOR borderColor;
            void init();
        };
    }
}


#endif //ROWS_RECTANGLE_H
