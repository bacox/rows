//
// Created by bart on 11-1-19.
//

#include "Cross.h"
#include "../../Engine/Draw.h"

void Game::Drawable::Cross::draw() {
    Base_Element::draw();
    // Draw Cross
    Engine::Position p2(pos.getX() + dim.getWidth(), pos.getY());
    Engine::Position p3(pos.getX(), pos.getY() + dim.getHeight());
    Engine::Position p4(pos.getX() + dim.getWidth(), pos.getY() + dim.getHeight());

    Engine::Draw::Line(pos, p4, color, 6);
    Engine::Draw::Line(p2, p3, color, 6);
}

Game::Drawable::Cross::Cross(const Engine::Position &pos, const Engine::Dimension &dim) : Grid_Element(pos, dim) {
    type = "Cross";
}

Game::Drawable::Cross::Cross(int x, int y, int w, int h) : Grid_Element(x, y, w, h) {

}
