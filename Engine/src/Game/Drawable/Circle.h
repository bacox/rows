//
// Created by bart on 6-1-19.
//

#ifndef ROWS_CIRCLE_H
#define ROWS_CIRCLE_H

#include "../../epzh.h"
#include "../../Engine/Base_Element.h"
#include "../Grid_Element.h"

namespace Game {
    namespace Drawable {
    class Circle : public Grid_Element {

    public:
        Circle(const Engine::Position &pos, const Engine::Dimension &dim);
        Circle(int x, int y, int w, int h);

        void draw() override;
    };
    }
}



#endif //ROWS_CIRCLE_H
