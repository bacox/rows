//
// Created by bart on 12-1-19.
//

#include "Tile.h"
#include "../../Engine/Draw.h"

Game::Tile::Tile(const Engine::Position &pos, const Engine::Dimension &dim) : Grid_Element(pos, dim) {
    type = "Tile";
}

void Game::Tile::draw() {
    Base_Element::draw();

    ALLEGRO_COLOR tileColor = color;
    if(isHover()) {
        tileColor = hoverColor;
    }

    Engine::Draw::RectangleFilled(pos, dim, tileColor);
}

Game::Tile::Tile(int x, int y, int w, int h) : Grid_Element(x, y, w, h) {
    type = "Tile";
}
