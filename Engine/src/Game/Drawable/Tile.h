//
// Created by bart on 12-1-19.
//

#ifndef ROWS_TILE_H
#define ROWS_TILE_H

#include "../../epzh.h"
#include "../../Engine/Base_Element.h"
#include "../Grid_Element.h"

namespace Game {
class Tile : public Grid_Element {
    public:
        Tile(const Engine::Position &pos, const Engine::Dimension &dim);
        Tile(int x, int y, int w, int h);

        void draw() override;
    private:
        ALLEGRO_COLOR color = al_map_rgb(164, 166, 168); // Grey
        int hoverAddition = 50;
        ALLEGRO_COLOR hoverColor = al_map_rgb(164+hoverAddition, 166+hoverAddition, 168 +hoverAddition);
    };
}


#endif //ROWS_TILE_H
