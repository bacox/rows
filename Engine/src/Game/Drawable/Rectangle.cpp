//
// Created by bacox on 24-5-19.
//

#include "Rectangle.h"
#include "../../Engine/Draw.h"


Game::Drawable::Rectangle::Rectangle(const Engine::Position &pos, const Engine::Dimension &dim) : Base_Element(pos, dim) {
    init();
}

Game::Drawable::Rectangle::Rectangle(int x, int y, int w, int h) : Base_Element(x, y, w, h) {
    init();
}

void Game::Drawable::Rectangle::init() {
    backgroundColor = al_map_rgb(0,0,0);
    borderColor = al_map_rgb(255,255,255);
}


void Game::Drawable::Rectangle::setBackgroundColor(const ALLEGRO_COLOR &c) {
    backgroundColor = c;
}

void Game::Drawable::Rectangle::setBorderColor(const ALLEGRO_COLOR &c) {
    borderColor = c;
}

void Game::Drawable::Rectangle::draw() {
    Base_Element::draw();

    Engine::Draw::RectangleFilled(pos, dim, backgroundColor);
}
