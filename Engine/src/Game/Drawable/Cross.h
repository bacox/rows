//
// Created by bart on 11-1-19.
//

#ifndef ROWS_CROSS_H
#define ROWS_CROSS_H

#include "../../epzh.h"
#include "../../Engine/Types/Dimension.h"
#include "../../Engine/Base_Element.h"
#include "../Grid_Element.h"

namespace Game {
    namespace  Drawable {
        class Cross : public Grid_Element {
        public:
            Cross(const Engine::Position &pos, const Engine::Dimension &dim);
            Cross(int x, int y, int w, int h);

            void draw() override;

        };
    }
}


#endif //ROWS_CROSS_H
