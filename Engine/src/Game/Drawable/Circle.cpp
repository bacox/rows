//
// Created by bart on 6-1-19.
//

#include "Circle.h"
#include "../../Engine/Draw.h"

void Game::Drawable::Circle::draw() {
    type = "Circle";
    Base_Element::draw();
    Engine::Position center(pos.getX() + (dim.getWidth()/2), pos.getY() + (dim.getHeight() /2));
    int lineWidth = 6;
    Engine::Draw::Circle(center.getX(), center.getY(), (dim.getWidth()/2) - (lineWidth / 2), color, lineWidth);
}

Game::Drawable::Circle::Circle(const Engine::Position &pos, const Engine::Dimension &dim) : Grid_Element(pos, dim) {
    type = "Circle";
}

Game::Drawable::Circle::Circle(int x, int y, int w, int h) : Grid_Element(x, y, w, h) {
    type = "Circle";
}

