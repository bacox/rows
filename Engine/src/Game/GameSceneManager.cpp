//
// Created by bacox on 22-5-19.
//

#include "GameSceneManager.h"
#include "Scenes/StartScene.h"
#include "Scenes/GameScene.h"


void Game::GameSceneManager::registerAllScenes() {
    Engine::Base_Scene * startScene = new Game::Scene::StartScene();
    Engine::Base_Scene * mainGame = new Game::Scene::GameScene();

    setScene("start", startScene);
    setScene("mainGame", mainGame);
    setCurrent("start");
}

Game::GameSceneManager &Game::GameSceneManager::getInstance() {
    static GameSceneManager instance; // Guaranteed to be destroyed.

    // Instantiated on first use.
    return instance;
}

Game::GameSceneManager::GameSceneManager() {

}
