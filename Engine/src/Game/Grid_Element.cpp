//
// Created by bart on 24-1-19.
//

#include "Grid_Element.h"

Game::Grid_Element::Grid_Element(const Engine::Position &pos, const Engine::Dimension &dim) : Base_Element(pos, dim) {
}

void Game::Grid_Element::setColor(const ALLEGRO_COLOR &color) {
    Grid_Element::color = color;
}

const ALLEGRO_COLOR &Game::Grid_Element::getColor() const {
    return color;
}

Game::Grid_Element::Grid_Element(int x, int y, int w, int h) : Base_Element(x, y, w, h) {

}

void Game::Grid_Element::getCentralPoint(Engine::Position &position) {
    int x = pos.getX() + (dim.getWidth() / 2);
    int y = pos.getY() + (dim.getHeight() / 2);
    position.move(x, y);
}
