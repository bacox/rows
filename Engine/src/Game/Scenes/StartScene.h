//
// Created by bacox on 8-5-19.
//

#ifndef ROWS_STARTSCENE_H
#define ROWS_STARTSCENE_H


#include <vector>
#include "../../Engine/Base_Scene.h"
#include "../../Engine/Base_Element.h"
#include "../../Engine/Layer.h"

namespace Game {
    namespace Scene {
    class StartScene : public Engine::Base_Scene {
        private:
            Engine::Layer backLayer;
            Engine::Layer foregroundLayer;
            std::vector<Engine::Base_Element *> ui_elements;

        public:
            void load() override;

            void unload() override;

            void render() override;

            void tearDown() override;

            void * getElementFromScene(Engine::Position p) override;
    };
    }
}


#endif //ROWS_STARTSCENE_H
