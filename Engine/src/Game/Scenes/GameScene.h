//
// Created by bacox on 22-5-19.
//

#ifndef ROWS_GAMESCENE_H
#define ROWS_GAMESCENE_H

#include "../../Engine/Base_Scene.h"
#include "../../Engine/Layer.h"
#include "../../Engine/UI/Text.h"


namespace Game {
    namespace Scene {
        class GameScene : public Engine::Base_Scene {
        private:
            Engine::Layer backLayer;
            Engine::Layer foregroundLayer;
            Engine::UI::Text * score;
        public:

            void showWinner();
            void load() override;

            void unload() override;

            void render() override;

            void tearDown() override;

            void *getElementFromScene(Engine::Position p) override;
        };
    }
}


#endif //ROWS_GAMESCENE_H
