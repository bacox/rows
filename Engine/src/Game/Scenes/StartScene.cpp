//
// Created by bacox on 8-5-19.
//

#include "StartScene.h"
#include "../../Engine/UI/Button.h"
#include "../GameSceneManager.h"
#include "../../Engine/UI/Text.h"
#include "../Drawable/Rectangle.h"
#include "../../Engine/GameObject.h"

void Game::Scene::StartScene::load() {
    Game::Drawable::Rectangle * background = new Game::Drawable::Rectangle(0, 0, 800, 800);
    background->setBackgroundColor(al_map_rgb(150, 150, 150));

    Engine::UI::Button * btn = new Engine::UI::Button("Start", 325, 300, 150, 60);
    btn->setCallback([](const Engine::Event * ev){
       GameSceneManager &manager = GameSceneManager::getInstance();
       manager.transitionTo("mainGame");
    });

    Engine::UI::Button * exitBtn = new Engine::UI::Button("Exit", 325, 400, 150, 60);
    exitBtn->setCallback([](const Engine::Event * ev){
        Engine::GameObject::running = false;
    });

    Engine::UI::Text * header = new Engine::UI::Text("Rows", 400, 50, 800, 100);
    header->setFontsize(100);

    Engine::UI::Text * footer = new Engine::UI::Text("Bart Cox - 2019", 400, 750, 800, 100);

    backLayer.addElement(background);

    foregroundLayer.addElement(btn);
    foregroundLayer.addElement(exitBtn);

    foregroundLayer.addElement(header);
    foregroundLayer.addElement(footer);



}

void Game::Scene::StartScene::unload() {
    foregroundLayer.destroyAllElements();
    backLayer.destroyAllElements();
}

void Game::Scene::StartScene::render() {

    backLayer.draw();
    foregroundLayer.draw();
}

void Game::Scene::StartScene::tearDown() {

}

void * Game::Scene::StartScene::getElementFromScene(Engine::Position p) {

    Engine::Base_Element * result = nullptr;
    result = foregroundLayer.hasElement(p);
    if(!result) {
        result = backLayer.hasElement(p);
    }
    return result;
}
