//
// Created by bacox on 22-5-19.
//

#include "GameScene.h"
#include "../../Engine/UI/Button.h"
#include "../../Engine/UI/Text.h"
#include "../GameSceneManager.h"
#include "../Drawable/Tile.h"
#include "../Grid.h"


void Game::Scene::GameScene::load() {
    Engine::UI::Button * btn = new Engine::UI::Button("Back", -1, -1, 150, 60);
    btn->setCallback([](const Engine::Event * ev){
        GameSceneManager &manager = GameSceneManager::getInstance();
        manager.transitionTo("start");
    });


    score = new Engine::UI::Text("Winner", 400, 0, 800, 100);
    score->setFontsize(75);
    score->setHidden(true);



    int tileSize = 190;
    Tile * t1 = new Tile(5, 5, tileSize,tileSize);
    Tile * t2 = new Tile(205, 5, 190,190);
    Tile * t3 = new Tile(405, 5, 190,190);
    Tile * t4 = new Tile(5, 205, 190,190);
    Tile * t5 = new Tile(205, 205, 190,190);
    Tile * t6 = new Tile(405, 205, 190,190);
    Tile * t7 = new Tile(5, 405, 190,190);
    Tile * t8 = new Tile(205, 405, 190,190);
    Tile * t9 = new Tile(405, 405, 190,190);

    Grid * grid = new Grid(115, 115, 0, 0, 190, 190);

    grid->setVoidCallback([](){
        EN_CORE_INFO("We got a winner!");
         GameScene * scene = dynamic_cast<GameScene *>(Game::GameSceneManager::getInstance().getSceneById("mainGame"));
        scene->showWinner();
    });


    backLayer.addElement(grid);
    grid->addElement(0, 0, t1);
    grid->addElement(0, 1, t2);
    grid->addElement(0, 2, t3);
    grid->addElement(1, 0, t4);
    grid->addElement(1, 1, t5);
    grid->addElement(1, 2, t6);
    grid->addElement(2, 0, t7);
    grid->addElement(2, 1, t8);
    grid->addElement(2, 2, t9);

    t1->addListener(grid);
    t2->addListener(grid);
    t3->addListener(grid);
    t4->addListener(grid);
    t5->addListener(grid);
    t6->addListener(grid);
    t7->addListener(grid);
    t8->addListener(grid);
    t9->addListener(grid);

    foregroundLayer.addElement(btn);
    foregroundLayer.addElement(score);
}

void Game::Scene::GameScene::unload() {
    foregroundLayer.destroyAllElements();
    backLayer.destroyAllElements();
}

void Game::Scene::GameScene::render() {
    backLayer.draw();
    foregroundLayer.draw();
}

void Game::Scene::GameScene::tearDown() {

}

void *Game::Scene::GameScene::getElementFromScene(Engine::Position p) {
    Engine::Base_Element * result = nullptr;
    result = foregroundLayer.hasElement(p);
    if(!result) {
        result = backLayer.hasElement(p);
    }
    return result;
}

void Game::Scene::GameScene::showWinner() {
    score->setHidden(false);
}
