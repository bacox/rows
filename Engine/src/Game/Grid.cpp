//
// Created by bart on 24-1-19.
//

#include "Grid.h"
#include "Drawable/Circle.h"
#include "Drawable/Cross.h"
#include "../Engine/Draw.h"

Game::Grid::Grid(const Engine::Position &pos, const Engine::Dimension &dim, const Engine::Dimension &grid_element_dim) : Base_Element(pos, dim) {
    type = "Grid";
    nested = true;
    element_size.resize(grid_element_dim.getWidth(), grid_element_dim.getHeight());

    this->dim.resize(num_horizontal * element_size.getWidth(), num_vertical * element_size.getHeight());
    grid.reserve(num_vertical * num_horizontal);
    grid.resize(num_vertical * num_horizontal);
}


Game::Grid::Grid(int x, int y, int w, int h, int gridWidth, int gridHeight) : Base_Element(x, y, w, h){
    type = "Grid";
    nested = true;
    element_size.resize(gridWidth, gridHeight);

    this->dim.resize(num_horizontal * element_size.getWidth(), num_vertical * element_size.getHeight());
    grid.reserve(num_vertical * num_horizontal);
    grid.resize(num_vertical * num_horizontal);
}

void Game::Grid::replaceElement(int x, int y, Game::Grid_Element * newElement) {
    Grid_Element* old = grid[((y) * num_horizontal) + x];
    if(old)
    {
        delete old;
    }
//    grid[((y) * num_horizontal) + x] = newElement;
    addElement(x, y, newElement);
}

void Game::Grid::draw() {
    Base_Element::draw();
    for(Grid_Element * el : grid)
    {
        if(el)
        {
            el->draw();
        }
    }

    // Draw score
    drawScore();
    if(hasWinner()) {
        if(void_callback){
            void_callback();
        }
    }
}


void Game::Grid::addElement(int x, int y, Game::Grid_Element *element) {
    EN_CORE_INFO("Adding new element to grid at " + std::to_string(((y) * num_horizontal) + x));

    element->move(this->pos.getX() + (x * element_size.getWidth()), this->pos.getY() + (y * element_size.getHeight()));
    element->grid_pos_x = x;
    element->grid_pos_y = y;
    grid[((y) * num_horizontal) + x] = element;
    std::cout << grid.size() << std::endl;

}

bool Game::Grid::hasPosition(Engine::Position p) {
    return Base_Element::hasPosition(p);
}

Engine::Base_Element *Game::Grid::getNestedPosition(Engine::Position p) {
//    return Base_Element::getNestedPosition(p);

    for(Grid_Element *el : grid) {
//        std::cout << el << std::endl;
        if(el){
            if(el->hasPosition(p)) {
                return el;
            }
        }

    }
    return this;
}

void Game::Grid::sendUpdate(Engine::Event *event) {
    EN_CORE_INFO("I got an event in grid");

    // Check if a tile of ours is clicked

    for(Grid_Element * el: grid)
    {
        if(event->clickedElement == el)
        {
            EN_CORE_INFO("A tile of mine is clicked");
            std::cout << el << std::endl;
            int x = el->grid_pos_x;
            int y = el->grid_pos_y;
            Engine::Position * pos = new Engine::Position(0,0);
            Engine::Dimension * dim = new Engine::Dimension(element_size.getWidth(), element_size.getHeight());
            Grid_Element * newElement;
            if(drawCircle)
            {
                newElement = new Drawable::Circle(*pos,*dim);
                newElement->setColor(al_map_rgb(255,0,0));
                EN_CORE_INFO("Draw red circle");
            } else
            {
                newElement = new Drawable::Cross(*pos,*dim);
                newElement->setColor(al_map_rgb(0,255,0));
                EN_CORE_INFO("Draw blue circle");
            }
            drawCircle = !drawCircle;
            replaceElement(x, y, newElement);

            //Check score
            checkScore();
        }
    }
}

void Game::Grid::checkScore() {
    std::vector<int> grid_values;
    for(Grid_Element * el : grid) {
        int value = 0;
        if(el->getType() == "Circle") {
            value = 1;
        } else if(el->getType() == "Cross") {
            value = -1;
        }
        grid_values.push_back(value);
    }

    // Check horizontal
    int h_top = grid_values[0] + grid_values[1] + grid_values[2];
    int h_middle = grid_values[3] + grid_values[4] + grid_values[5];
    int h_bottom = grid_values[6] + grid_values[7] + grid_values[8];
    int v_left = grid_values[0] + grid_values[3] + grid_values[6];
    int v_middle = grid_values[1] + grid_values[4] + grid_values[7];
    int v_right = grid_values[2] + grid_values[5] + grid_values[8];

    int d_top = grid_values[0] + grid_values[4] + grid_values[8];
    int d_bottom = grid_values[6] + grid_values[4] + grid_values[2];

    if(h_top == 3) {
        draw_scores[0] = 1;
        winner = 1;
    }
    if(h_top == -3) {
        draw_scores[0] = 1;
        winner = 2;
    }
    if(h_middle == 3) {
        draw_scores[1] = 1;
        winner = 1;
    }
    if(h_middle == -3) {
        draw_scores[1] = 1;
        winner = 2;
    }
    if(h_bottom == 3) {
        draw_scores[2] = 1;
        winner = 1;
    }
    if(h_bottom == -3) {
        draw_scores[2] = 1;
        winner = 2;
    }
    if(v_left == 3) {
        draw_scores[3] = 1;
        winner = 1;
    }
    if(v_left == -3) {
        draw_scores[3] = 1;
        winner = 2;
    }
    if(v_middle == 3) {
        draw_scores[4] = 1;
        winner = 1;
    }
    if(v_middle == -3) {
        draw_scores[4] = 1;
        winner = 2;
    }
    if(v_right == 3) {
        draw_scores[5] = 1;
        winner = 1;
    }
    if(v_right == -3) {
        draw_scores[5] = 1;
        winner = 2;
    }
    if(d_top == 3) {
        draw_scores[6] = 1;
        winner = 1;
    }
    if(d_top == -3) {
        draw_scores[6] = 1;
        winner = 2;
    }
    if(d_bottom == 3) {
        draw_scores[7] = 1;
        winner = 1;
    }
    if(d_bottom == -3) {
        draw_scores[7] = 1;
        winner = 2;
    }


}

void Game::Grid::drawScore() {
    if(draw_scores[0]) {
        // Draw top
        Engine::Position start;
        grid[0]->getCentralPoint(start);
        Engine::Position end;
        grid[2]->getCentralPoint(end);

        draw_score_line(start, end);
    }

    if(draw_scores[1]) {
        // Draw top
        Engine::Position start;
        grid[3]->getCentralPoint(start);
        Engine::Position end;
        grid[5]->getCentralPoint(end);

        draw_score_line(start, end);
    }

    if(draw_scores[2]) {
        // Draw top
        Engine::Position start;
        grid[6]->getCentralPoint(start);
        Engine::Position end;
        grid[8]->getCentralPoint(end);

        draw_score_line(start, end);
    }

    if(draw_scores[3]) {
        // Draw top
        Engine::Position start;
        grid[0]->getCentralPoint(start);
        Engine::Position end;
        grid[6]->getCentralPoint(end);

        draw_score_line(start, end);
    }

    if(draw_scores[4]) {
        // Draw top
        Engine::Position start;
        grid[1]->getCentralPoint(start);
        Engine::Position end;
        grid[7]->getCentralPoint(end);

        draw_score_line(start, end);
    }

    if(draw_scores[5]) {
        // Draw top
        Engine::Position start;
        grid[2]->getCentralPoint(start);
        Engine::Position end;
        grid[8]->getCentralPoint(end);

        draw_score_line(start, end);
    }

    if(draw_scores[6]) {
        // Draw top
        Engine::Position start;
        grid[0]->getCentralPoint(start);
        Engine::Position end;
        grid[8]->getCentralPoint(end);

        draw_score_line(start, end);
    }

    if(draw_scores[7]) {
        // Draw top
        Engine::Position start;
        grid[6]->getCentralPoint(start);
        Engine::Position end;
        grid[2]->getCentralPoint(end);

        draw_score_line(start, end);
    }
}

void Game::Grid::draw_score_line(const Engine::Position &start, const Engine::Position &end) {
    Engine::Draw::Line(start.getX(), start.getY(), end.getX(), end.getY(), al_map_rgb(255,255,255), 10);
}

bool Game::Grid::hasWinner() {
    return winner > 0;
}

int Game::Grid::getWinner() {
    return winner;
}

void Game::Grid::setVoidCallback(std::function<void()> f) {
    void_callback = f;
}

