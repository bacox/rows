//
// Created by bart on 24-1-19.
//

#ifndef ROWS_GRID_ELEMENT_H
#define ROWS_GRID_ELEMENT_H

#include "../epzh.h"
#include "../Engine/Base_Element.h"

namespace Game {
class Grid_Element : public Engine::Base_Element{
public:
    int grid_pos_x = 0;
    int grid_pos_y = 0;
    void setColor(const ALLEGRO_COLOR &color);

protected:
    ALLEGRO_COLOR color;
public:
    const ALLEGRO_COLOR &getColor() const;

public:
    Grid_Element(const Engine::Position &pos, const Engine::Dimension &dim);
    Grid_Element(int x, int y, int w, int h);
    void getCentralPoint(Engine::Position &pos);
};
}


#endif //ROWS_GRID_ELEMENT_H
