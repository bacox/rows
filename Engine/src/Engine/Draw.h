//
// Created by bart on 5-1-19.
//

#ifndef ROWS_DRAW_H
#define ROWS_DRAW_H

#include "../epzh.h"
#include "Types/Position.h"
#include "Types/Dimension.h"

namespace Engine {
    class Draw {
    public:
        static void Rectangle(int x1, int y1, int x2, int y2, ALLEGRO_COLOR color, int thickness);
        static void Rectangle(Position p1, Position p2, ALLEGRO_COLOR color, int thickness);
        static void Rectangle(Position p, Dimension d, ALLEGRO_COLOR color, int thickness);
        static void RectangleFilled(int x1, int y1, int x2, int y2, ALLEGRO_COLOR color);
        static void RectangleFilled(Position p1, Position p2, ALLEGRO_COLOR color);
        static void RectangleFilled(Position p, Dimension d, ALLEGRO_COLOR color);

        static void Line(int x1, int y1, int x2, int y2, ALLEGRO_COLOR color, int thickness);
        static void Line(Position p1, Position p2, ALLEGRO_COLOR color, int thickness);
        static void Line(Position p, Dimension d, ALLEGRO_COLOR color, int thickness);

        static void Circle(float cx, float cy, float r, ALLEGRO_COLOR color, float thickness);
        static void Circle(Position p, float r, ALLEGRO_COLOR color, float thickness);
    };
}


#endif //ROWS_DRAW_H
