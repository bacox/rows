//
// Created by bacox on 15-5-19.
//

#ifndef ROWS_BASE_SCENE_H
#define ROWS_BASE_SCENE_H

#include "Base_Element.h"

namespace Engine {

    class Base_Scene {
    public:
        virtual void load() = 0;

        virtual void unload() = 0;

        virtual void render() = 0;

        virtual void tearDown() = 0;

        virtual void * getElementFromScene(Position p) = 0;
    };
}
#endif //ROWS_BASE_SCENE_H
