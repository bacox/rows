//
// Created by bart on 6-1-19.
//

#ifndef ROWS_BASE_ELEMENT_H
#define ROWS_BASE_ELEMENT_H

#include "../epzh.h"
#include "Types/Position.h"
#include "Types/Dimension.h"
#include "Listener.h"

namespace Engine {
    class Base_Element {
    protected:
        Position pos;
        Dimension dim;
        std::string type = "Base Element";
        ALLEGRO_COLOR hoverColor = al_map_rgb(255, 255, 255);
        bool nested = false;
        std::vector<Engine::Listener*> listeners;
    public:
        bool isHidden() const;

        void setHidden(bool hidden);

    public:
        bool isNested() const;

    private:
        bool hidden = false;
        bool hover = false;
        std::function<void(const Event*)> subscriber_callback_;
    public:
        bool isHover() const;
        void setHover(bool hover);

        void move(int x, int y);

    private:
        ALLEGRO_COLOR boundingBox = al_map_rgb(255, 0, 0);
        bool drawBoundingBox = false;
    public:
        void setDrawBoundingBox(bool drawBoundingBox);

        virtual bool hasPosition(Position p);

        virtual Base_Element * getNestedPosition(Position p);
        const std::string &getType() const;

        void addListener(Listener * listener);
        void mouseClickEvent(Event * ev);

        void setCallback(std::function<void(const Event*)>);


    public:
        Base_Element(const Position &pos, const Dimension &dim);
        Base_Element(int x, int y, int w, int h);
        virtual void draw();

        virtual ~Base_Element();
    };

}


#endif //ROWS_BASE_ELEMENT_H
