//
// Created by bart on 13-1-19.
//

#include "Hud.h"
#include "Draw.h"

Engine::Hud::Hud() {

}

void Engine::Hud::updateMouseEvent(ALLEGRO_EVENT * event) {
    mouse_x = event->mouse.x;
    mouse_y = event->mouse.y;
    mouse_dx = event->mouse.dx;
    mouse_dy = event->mouse.dy;
}

void Engine::Hud::draw() {

    std::map<std::string, std::string> hudItems;
    hudItems["Current element"] = currentElementHover;
    hudItems["mouse_dy"] = std::to_string(mouse_dy);
    hudItems["mouse_dx"] = std::to_string(mouse_dx);
    hudItems["mouse_y"] = std::to_string(mouse_y);
    hudItems["mouse_x"] = std::to_string(mouse_x);
    hudItems["FPS"] = std::to_string(fps);

    int indent = 10;
    int longestText = 0;
    // Determine the longest text width
    for (auto &hudItem : hudItems) {
        longestText = std::max( longestText, al_get_text_width(font, hudItem.first.c_str()));
    }
    int stepCounter = 0;
    // Draw hud values
    for (auto &hudItem : hudItems) {
        al_draw_text(font, hudColor, 0, 0 + (fontsize * stepCounter), ALLEGRO_ALIGN_LEFT, hudItem.first.c_str());
        al_draw_text(font, hudColor, longestText + indent, 0 + (fontsize * stepCounter++), ALLEGRO_ALIGN_LEFT,
                hudItem.second.c_str());
    }
}

void Engine::Hud::init() {

    hudColor = al_map_rgb(0, 255, 0);
    ALLEGRO_PATH *path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
    EN_CORE_INFO(al_path_cstr(path, '/'));
    font = al_load_ttf_font("resources/fonts/OpenSans-Regular.ttf",fontsize,0 );
    if(!font) {
        EN_CORE_ERROR("Unable ot initialize hud font");
        throw "Unable ot initialize hud font";
    }
    EN_CORE_INFO("Hub font loaded correctly");
}

void Engine::Hud::updateFPS(int f) {
    fps = f;
}

void Engine::Hud::updateCurrentElementName(Engine::Base_Element *el) {
    if(el) {
        currentElementHover = el->getType();
        return;
    }
    currentElementHover = "none";
}
