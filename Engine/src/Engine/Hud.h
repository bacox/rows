//
// Created by bart on 13-1-19.
//

#ifndef ROWS_HUD_H
#define ROWS_HUD_H

#include <events.h>
#include "../epzh.h"
#include "Base_Element.h"

namespace Engine {
    class Hud {
        int mouse_x = 0;
        int mouse_y = 0;
        int mouse_dx = 0;
        int mouse_dy = 0;
        int fps = 0;
        std::string currentElementHover = "none";
        ALLEGRO_COLOR hudColor;
        int fontsize = 10;
        ALLEGRO_FONT *font;

    public:
        Hud();
        void init();
        void updateCurrentElementName(Base_Element * el);
        void updateMouseEvent(ALLEGRO_EVENT * event);
        void updateFPS(int f);
        void draw();
    };
}


#endif //ROWS_HUD_H
