//
// Created by bart on 6-1-19.
//

#ifndef ROWS_GAMEOBJECT_H
#define ROWS_GAMEOBJECT_H

#include "../epzh.h"
#include "Base_Element.h"
#include "Window.h"
#include "Layer.h"
#include "Hud.h"
#include "Mouse.h"
#include "Base_Scene.h"
#include "../Game/GameSceneManager.h"


namespace Engine {
    class GameObject {
    private:
        std::vector<Base_Element*> elements;
        const float FPS = 60;
        Layer backLayer;
        Layer foregroundLayer;
        Engine::Hud hud;
//        Base_Element * element = NULL;
        int fps_counter = 0;
        double fps_timer = 0;
        Mouse mouse;

        Game::GameSceneManager& sceneCoordinator = Game::GameSceneManager::getInstance();

    public:
        const std::shared_ptr<Window> &getWindow() const;
        static bool GLOBAL_DEBUG;
        static bool running;

    private:
//        Window * window;
        std::shared_ptr<Engine::Window> window;
        ALLEGRO_EVENT_QUEUE *main_event_queue;
        ALLEGRO_TIMER *main_timer;
        bool redraw = false;
    public:
        GameObject();
        void init();
        void initTimer();
        void initQueue();
        bool isRunning();
        void start();
        Layer * getBackLayer();
        Layer * getFrontLayer();
        void addElement(Base_Element * el);
        void createWindow(int width, int height);
        void run();

        Engine::Base_Element *getElementFromLayer(Position p);
        Engine::Base_Element *getElementFromScene(Position p);

        virtual ~GameObject();

    private:
        void resetFpsCounter();
        void handleEvents();
        void handleRedraw();
    };
}


#endif //ROWS_GAMEOBJECT_H
