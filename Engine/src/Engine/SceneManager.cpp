//
// Created by bacox on 7-5-19.
//

#include "SceneManager.h"

SceneManager::SceneManager() {
}

SceneManager &SceneManager::getInstance() {
    static SceneManager instance; // Guaranteed to be destroyed.

    // Instantiated on first use.
    return instance;
}
