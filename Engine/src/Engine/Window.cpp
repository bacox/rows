//
// Created by bart on 1-1-19.
//

#include "Window.h"


int Engine::Window::init_display() {

    // Check if allegro is initialized
    if (!al_is_system_installed()) {
        EN_CORE_ERROR("Allegro not initialized.");
        return 1;
    }

    // Create the display
    display = al_create_display(width, height);
    if (!display) {
        EN_CORE_ERROR("Failed to create display.");
        return 1;
    }
    EN_CORE_INFO("Display created.");
    return 0;
}

Engine::Window::Window(int height, int width) : height(height), width(width) {
    if (init_display()) {
        EN_ERROR("Failed to create display");
    } else {
        EN_INFO("Window initialized");
    }
}

Engine::Window::~Window() {
    if (display) { // Do nothing if display is NULL
        EN_CORE_INFO("Desctructing window");
        al_destroy_display(display);
    }
}

ALLEGRO_DISPLAY *Engine::Window::getDisplay() const {
    return display;
}
