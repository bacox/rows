//
// Created by bart on 18-1-19.
//

#ifndef ROWS_MOUSE_H
#define ROWS_MOUSE_H

#include "../epzh.h"
#include "Base_Element.h"

namespace Engine{
    class Mouse {
    private:
        int x = 0;
        int y = 0;
        int dx = 0;
        int dy = 0;
        Base_Element * element = nullptr;

    public:
        void updateMousePosition(int x, int y, int dx, int dy);
        void changeElement(Base_Element * el);
    };
}



#endif //ROWS_MOUSE_H
