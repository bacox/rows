//
// Created by bart on 6-2-19.
//

#ifndef ROWS_EVENT_H
#define ROWS_EVENT_H

#include "Base_Element.h"
#include "Types/Position.h"
//#include "Base_Element.h"


namespace Engine {
    class Base_Element;
    enum EVENT_TYPE { MOUSE_EVENT };
    class Event {
    public:
        EVENT_TYPE event_type = MOUSE_EVENT;
        Engine::Position pos = Position(0,0);
        Engine::Base_Element * clickedElement;
    };
}

#endif //ROWS_EVENT_H

