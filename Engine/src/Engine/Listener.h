//
// Created by bart on 6-2-19.
//

#ifndef ROWS_LISTENER_H
#define ROWS_LISTENER_H


#include "Event.h"

namespace Engine {
    class Listener {
    public:
        virtual void sendUpdate(Engine::Event * event) = 0;
    };
}

#endif //ROWS_LISTENER_H
