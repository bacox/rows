//
// Created by bacox on 22-5-19.
//

#include "Text.h"
#include "../Draw.h"

Engine::UI::Text::Text(const std::string &text, const Engine::Position &pos, const Engine::Dimension &dim) : Base_Element(pos, dim), text(text){
    type = "Text";
    backgroundColor = al_map_rgb(0,0,255);
    textColor = al_map_rgb(255,0,0);
    ALLEGRO_PATH *path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
    EN_CORE_INFO(al_path_cstr(path, '/'));
    font = al_load_ttf_font("resources/fonts/OpenSans-Regular.ttf",fontsize,0 );
    if(!font) {
        EN_CORE_ERROR("Unable to initialize hud font");
        throw "Unable to initialize hud font";
    }
    EN_CORE_INFO("Text font loaded correctly");
    setDrawBoundingBox(false);
    generateLayout();
}

Engine::UI::Text::Text(const std::string &text, int x, int y, int w, int h) : Base_Element(x, y, w, h), text(text) {
    type = "Text";
    backgroundColor = al_map_rgb(0,0,255);
    textColor = al_map_rgb(255,0,0);
    ALLEGRO_PATH *path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
    EN_CORE_INFO(al_path_cstr(path, '/'));
    font = al_load_ttf_font("resources/fonts/OpenSans-Regular.ttf",fontsize,0 );
    if(!font) {
        EN_CORE_ERROR("Unable to initialize hud font");
        throw "Unable to initialize hud font";
    }
    EN_CORE_INFO("Text font loaded correctly");
    setDrawBoundingBox(false);
    generateLayout();
}


void Engine::UI::Text::draw() {
    Base_Element::draw();

    int posx = pos.getX();// - (dim.getWidth()/2);
    int posy = pos.getY();// - (dim.getHeight()/2);
    Position tmp(posx, posy);
    if(drawBrackground)
        Draw::RectangleFilled(tmp, dim, backgroundColor);

    al_draw_text(font, textColor, tmp.getX(), tmp.getY() + padding, ALLEGRO_ALIGN_CENTER, text.c_str());

}

void Engine::UI::Text::setFontsize(int size) {
    fontsize = size;
    font = al_load_ttf_font("resources/fonts/OpenSans-Regular.ttf",fontsize,0 );
    generateLayout();
}

void Engine::UI::Text::generateLayout() {
    int textWidth = al_get_text_width(font, text.c_str());
    int textHeigth = al_get_font_line_height(font);
    int totalWidth = textWidth + (2 * padding);
    int totalHeight = textHeigth + ( 2 * padding);
    dim.resize(totalWidth, totalHeight);
}

void Engine::UI::Text::setPadding(int value) {
    padding = value;
    generateLayout();
}

void Engine::UI::Text::setBackgroundTransparency(bool value) {
    drawBrackground = value;
}

void Engine::UI::Text::updateText(const std::string t) {
    text = t;
    generateLayout();
}
