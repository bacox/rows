//
// Created by bart on 18-1-19.
//

#ifndef ROWS_BUTTON_H
#define ROWS_BUTTON_H

#include "../../epzh.h"
#include "../Types/Position.h"
#include "../Types/Dimension.h"
#include "../Base_Element.h"

namespace Engine {
    namespace UI {
        class Button : public Engine::Base_Element{
        private:
            std::string text;
            ALLEGRO_COLOR textColor;
            ALLEGRO_COLOR backgroundColor;
            int fontsize = 40;
            ALLEGRO_FONT *font;
            int borderWidth = 2;
        public:
            Button(const std::string &text, const Position &pos, const Dimension &dim);
            Button(const std::string &text, int x, int y, int w, int h);

            void draw() override;

            void setTextColor(ALLEGRO_COLOR c);
            void setBackgroundColor(ALLEGRO_COLOR c);
        };
    }
}


#endif //ROWS_BUTTON_H
