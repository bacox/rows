//
// Created by bart on 18-1-19.
//

#ifndef ROWS_TEXTINPUT_H
#define ROWS_TEXTINPUT_H

#include "../../epzh.h"
#include "../Base_Element.h"

namespace Engine {
    class TextInput : public Base_Element{
    private:
        ALLEGRO_COLOR textColor = al_map_rgb(0,0,0);
        ALLEGRO_COLOR backgroundColor = al_map_rgb(255,255,255);
        ALLEGRO_COLOR borderSelectedColor = al_map_rgb(0, 255, 0);

    public:
        TextInput(const Position &pos, const Dimension &dim);
    };
}


#endif //ROWS_TEXTINPUT_H
