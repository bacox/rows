//
// Created by bart on 18-1-19.
//

#include "TextInput.h"

Engine::TextInput::TextInput(const Engine::Position &pos, const Engine::Dimension &dim) : Base_Element(pos, dim) {
    type = "TextInput";
}
