//
// Created by bacox on 22-5-19.
//

#ifndef ROWS_TEXT_H
#define ROWS_TEXT_H

#include "../Base_Element.h"

namespace Engine {
    namespace UI {
        class Text : public Engine::Base_Element {
        private:
            std::string text;
            ALLEGRO_COLOR textColor;
            ALLEGRO_COLOR backgroundColor;
            int fontsize = 25;
            ALLEGRO_FONT * font;
            int padding = 10;
            bool drawBrackground = false;

            void generateLayout();
        public:
            Text(const std::string &text, const Position &pos, const Dimension &dim);
            Text(const std::string &text, int x, int y, int w, int h);

            void setFontsize(int size);
            void setPadding(int value);
            void updateText(const std::string t);

            void setBackgroundTransparency(bool value);

            void draw() override;
        };
    }
}


#endif //ROWS_TEXT_H
