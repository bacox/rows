//
// Created by bart on 18-1-19.
//

#include "Button.h"
#include "../Draw.h"

Engine::UI::Button::Button(const std::string &text, const Engine::Position &pos, const Engine::Dimension &dim)  : Base_Element(pos, dim), text(
        text){
    type = "Button";
    backgroundColor = al_map_rgb(0, 255, 0);
    textColor = al_map_rgb(0,0,255);
    ALLEGRO_PATH *path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
    EN_CORE_INFO(al_path_cstr(path, '/'));
    font = al_load_ttf_font("resources/fonts/OpenSans-Regular.ttf",fontsize,0 );
    if(!font) {
        EN_CORE_ERROR("Unable ot initialize hud font");
        throw "Unable to initialize hud font";
    }
    EN_CORE_INFO("Button font loaded correctly");
}

Engine::UI::Button::Button(const std::string &text, int x, int y, int w, int h)  : Base_Element(x, y, w, h), text(
        text){
    type = "Button";
    backgroundColor = al_map_rgb(0, 255, 0);
    textColor = al_map_rgb(0,0,255);
    ALLEGRO_PATH *path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
    EN_CORE_INFO(al_path_cstr(path, '/'));
    font = al_load_ttf_font("resources/fonts/OpenSans-Regular.ttf",fontsize,0 );
    if(!font) {
        EN_CORE_ERROR("Unable ot initialize hud font");
        throw "Unable to initialize hud font";
    }
    EN_CORE_INFO("Button font loaded correctly");
}

void Engine::UI::Button::draw() {
    Base_Element::draw();
    Draw::RectangleFilled(pos, dim, backgroundColor);
    ALLEGRO_COLOR borderColor = textColor;
    if(isHover()) {
        borderColor = hoverColor;
    } else {
        Draw::Rectangle(pos, dim, borderColor, borderWidth);

    }
    al_draw_text(font, textColor, pos.getX() + (dim.getWidth() / 2), pos.getY(), ALLEGRO_ALIGN_CENTER, text.c_str());
}

void Engine::UI::Button::setTextColor(ALLEGRO_COLOR c) {
    textColor = c;
}

void Engine::UI::Button::setBackgroundColor(ALLEGRO_COLOR c) {
    textColor = c;
}
