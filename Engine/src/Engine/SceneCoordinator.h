//
// Created by bacox on 22-5-19.
//

#ifndef ROWS_SCENECOORDINATOR_H
#define ROWS_SCENECOORDINATOR_H

#include <string>
#include <map>
#include "Base_Scene.h"

namespace Engine {
    class SceneCoordinator {
    private:

        std::map<std::string, Base_Scene *> scenes;
        Base_Scene * current = nullptr;

    protected:
        void setScene(std::string id, Base_Scene * scene);
        bool setCurrent(const std::string& id);
    public:
        Engine::Base_Scene * getSceneById(const std::string id);
        virtual ~SceneCoordinator();
        virtual void registerAllScenes() = 0;
        Base_Scene * getCurrentScene();
        void transitionTo(std::string id);
    };

}


#endif //ROWS_SCENECOORDINATOR_H
