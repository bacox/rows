//
// Created by bart on 13-1-19.
//

#ifndef ROWS_LAYER_H
#define ROWS_LAYER_H

#include "../epzh.h"
#include "Base_Element.h"

namespace Engine {
    class Layer {
    private:
        int z_level = 0;
        std::vector<Base_Element*> elements;
    public:
        void addElement(Base_Element * el);
        void draw();
        Base_Element * hasElement(Position p);
        void destroyAllElements();
    };
}


#endif //ROWS_LAYER_H
