//
// Created by bart on 1-1-19.
//

#ifndef ROWS_WINDOW_H
#define ROWS_WINDOW_H
#pragma once

#include "../epzh.h"

namespace Engine {
    class Window {

    public:
        Window(int height, int width);

        virtual ~Window();

        int init_display();

    private:
        int height;
        int width;
        ALLEGRO_DISPLAY *display;
    public:
        ALLEGRO_DISPLAY *getDisplay() const;
    };
}

#endif //ROWS_WINDOW_H
