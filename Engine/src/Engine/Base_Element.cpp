//
// Created by bart on 6-1-19.
//

#include "Base_Element.h"
#include "Draw.h"
#include "GameObject.h"

Engine::Base_Element::Base_Element(const Position &pos, const Dimension &dim) : pos(pos), dim(dim) {}
Engine::Base_Element::Base_Element(int x, int y, int w, int h) {
    pos.move(x,y);
    dim.resize(w,h);
}

void Engine::Base_Element::setDrawBoundingBox(bool drawBoundingBox) {
    Base_Element::drawBoundingBox = drawBoundingBox;
}


void Engine::Base_Element::draw() {
    if (hidden) return;
    if( Engine::GameObject::GLOBAL_DEBUG || drawBoundingBox) {
        Draw::Rectangle(pos, dim, boundingBox, 1);
    }
}

const std::string &Engine::Base_Element::getType() const {
    return type;
}

bool Engine::Base_Element::hasPosition(Engine::Position p) {
    if(isHidden())
        return false;
    if(p.getX() < pos.getX() || p.getY() < pos.getY()) {
        return false;
    }
    Position pos2(pos.getX() + dim.getWidth(), pos.getY() + dim.getHeight());
    if(p.getX() > pos2.getX() || p.getY() > pos2.getY()) {
        return false;
    }
    return true;
}

bool Engine::Base_Element::isHover() const {
    return hover;
}

void Engine::Base_Element::setHover(bool hover) {
    Base_Element::hover = hover;
}

void Engine::Base_Element::move(int x, int y) {
    pos.move(x, y);
}

bool Engine::Base_Element::isNested() const {
    return nested;
}

Engine::Base_Element *Engine::Base_Element::getNestedPosition(Engine::Position p) {
    return nullptr;
}

void Engine::Base_Element::addListener(Engine::Listener *listener) {
    listeners.push_back(listener);
}

void Engine::Base_Element::mouseClickEvent(Event * ev) {
    ev->clickedElement = this;
    for(Listener * l : listeners)
    {
        if(l)
        {
            l->sendUpdate(ev);
        }
    }
    if(subscriber_callback_)
        subscriber_callback_(ev);
}

Engine::Base_Element::~Base_Element() {
    EN_CORE_INFO("Deleting base element");
}

void Engine::Base_Element::setCallback(std::function<void(const Engine::Event*)> f) {
    subscriber_callback_ = f;
}

bool Engine::Base_Element::isHidden() const {
    return hidden;
}

void Engine::Base_Element::setHidden(bool hidden) {
    Base_Element::hidden = hidden;
}
