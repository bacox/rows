//
// Created by bart on 6-1-19.
//

#include "GameObject.h"
#include "../Game/Scenes/StartScene.h"

bool Engine::GameObject::GLOBAL_DEBUG = false;

bool Engine::GameObject::running = true;

void Engine::GameObject::run() {


    // Handle events
    handleEvents();
    // Compute changes

    handleRedraw();

    if(al_get_time() > fps_timer + 1.0) {
        hud.updateFPS(fps_counter);
        resetFpsCounter();
    }

}

void Engine::GameObject::addElement(Engine::Base_Element *el) {
    elements.push_back(el);
}

Engine::GameObject::GameObject() {
}

Engine::GameObject::~GameObject() {
    if(main_timer) {
        al_destroy_timer(main_timer);
    }
    if(window) {
        window.reset();
    }
}

void Engine::GameObject::createWindow(int width, int height) {
    window = std::make_shared<Engine::Window>(width, height);
//            new Window(width, height);
}

const std::shared_ptr<Engine::Window> &Engine::GameObject::getWindow() const {
    return window;
}

void Engine::GameObject::init() {
    EN_CORE_INFO("Initializing game");
    if (!al_init()) {
        EN_CORE_ERROR("Failed to initialize allegro.");
        throw "Failed to initialize allegro lib";
    }


    EN_CORE_INFO("Installing primitives addon");

    if(!al_init_primitives_addon()) {
        EN_CORE_ERROR("Failed to initialize the primitives addon");
        throw "Failed to initialize the primitives addon";
    }
    EN_CORE_INFO("Installing mouse");

    if(!al_install_mouse()) {
        EN_CORE_ERROR("Failed to initialize the mouse");
        throw "Failed to initialize the mouse";
    }


    EN_CORE_INFO("Installing keyboard");
    if(!al_install_keyboard()) {
        EN_CORE_ERROR("Failed to initialize the keyboard");
        throw "Failed to initialize the keyboard";
    }

    al_init_font_addon(); // initialize the font addon
    EN_CORE_INFO("Font addon initialized");
    al_init_ttf_addon();// initialize the ttf (True Type Font) addon
    EN_CORE_INFO("TTF addon initialized");

    initTimer();
    initQueue();
    hud.init();

    sceneCoordinator.getCurrentScene()->load();
//    currentScene = new Game::Scene::StartScene();
//    currentScene->load();
}

void Engine::GameObject::handleEvents() {
    ALLEGRO_EVENT event;
    ALLEGRO_TIMEOUT timeout;

    // Initialize timeout
    al_init_timeout(&timeout, 0.06);

    // Fetch the event (if one exists)
    bool get_event = al_wait_for_event_until(main_event_queue, &event, &timeout);


    Engine::Position mouse_pos(0,0);
    Base_Element * element;
//    std::cout << "Null ptr" << element << std::endl;
//    EN_CORE_INFO("Debug 1");
    // Handle event
    if(get_event) {
        switch (event.type) {
            case ALLEGRO_EVENT_TIMER:
                redraw = true;
                break;
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                running = false;
                break;

//            case ALLEGRO_EVENT_KEY_DOWN:
//                break;
//            case ALLEGRO_EVENT_KEY_UP:
//                break;
            case ALLEGRO_EVENT_KEY_CHAR:
                EN_CORE_INFO("Key was pressed");
                break;
            case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
            {
                // Mouse click
                std::stringstream ss;
                int mouse_x = event.mouse.x;
                int mouse_y = event.mouse.y;
                ss << "Mouse clicked at x: " << mouse_x << " y: " << mouse_y;
//                hud.updateMouseEvent(&event);
                EN_CORE_INFO(ss.str());

                // Get element at position
                mouse_pos.move(event.mouse.x, event.mouse.y);
                element = getElementFromScene(mouse_pos);
                if(element)
                {
                    EN_CORE_INFO("Clicked on element: " + element->getType());
                    Event ev;
                    ev.pos = mouse_pos;
                    element->mouseClickEvent(&ev);


                }


            }
                break;
            case ALLEGRO_EVENT_MOUSE_AXES:
                mouse.updateMousePosition(event.mouse.x, event.mouse.y, event.mouse.dx, event.mouse.dy);
                hud.updateMouseEvent(&event);
                mouse_pos.move(event.mouse.x, event.mouse.y);
                element = getElementFromScene(mouse_pos);
                mouse.changeElement(element);
                if(element) {
                    element->setHover(true);
                    al_set_system_mouse_cursor(window.get()->getDisplay(), ALLEGRO_SYSTEM_MOUSE_CURSOR_LINK);
                } else {
                    al_set_system_mouse_cursor(window.get()->getDisplay(), ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT);
                }
                hud.updateCurrentElementName(element);
                break;
            case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
                break;
        }
    }
}

bool Engine::GameObject::isRunning() {
    return running;
}

void Engine::GameObject::start() {
    running = true;
    al_start_timer(main_timer);

        // Register event sources
    al_register_event_source(main_event_queue, al_get_display_event_source(window->getDisplay()));
    al_register_event_source(main_event_queue, al_get_timer_event_source(main_timer));
    al_register_event_source(main_event_queue, al_get_mouse_event_source());
    al_register_event_source(main_event_queue, al_get_keyboard_event_source());

}

void Engine::GameObject::initTimer() {
    main_timer = al_create_timer(1.0 / FPS);
    if(!main_timer) {
        EN_CORE_ERROR("Unable to initialize timer");
        throw "Unable to initialize timer";
    }
    EN_CORE_INFO("Main timer initialized");
}

void Engine::GameObject::initQueue() {
    main_event_queue = al_create_event_queue();
    if (!main_event_queue) {
        EN_CORE_ERROR("Unable to create event queue");
        throw "Unable to create event queue";
    }
    EN_CORE_INFO("Event queue created");

}

void Engine::GameObject::handleRedraw() {
    if(redraw && al_is_event_queue_empty(main_event_queue)){
        fps_counter++;
        al_clear_to_color(al_map_rgb(0,0,0));

        sceneCoordinator.getCurrentScene()->render();
//        backLayer.draw();
//        foregroundLayer.draw();
//        hud.draw();

        // Flip the front and back buffer
        al_flip_display();
        redraw = false;
    }

}

Engine::Layer *Engine::GameObject::getBackLayer() {
    return &backLayer;
}

Engine::Layer *Engine::GameObject::getFrontLayer() {
    return &foregroundLayer;
}


void Engine::GameObject::resetFpsCounter() {
    fps_counter = 0;
    fps_timer = al_get_time();
}

Engine::Base_Element *Engine::GameObject::getElementFromLayer(Position p) {
    Base_Element * result = nullptr;
    result = foregroundLayer.hasElement(p);
    if(!result) {
        result = backLayer.hasElement(p);
    }
    return result;
}

Engine::Base_Element *Engine::GameObject::getElementFromScene(Position p) {
    Base_Element * result = nullptr;
    void * ptr = sceneCoordinator.getCurrentScene()->getElementFromScene(p);
    if(!ptr) {
        return result;
    }
    result = (Base_Element * ) ptr;
    return result;
}