//
// Created by bacox on 22-5-19.
//

#include "SceneCoordinator.h"

void Engine::SceneCoordinator::setScene(std::string id, Engine::Base_Scene * scene) {
    scenes[id] = scene;
}

bool Engine::SceneCoordinator::setCurrent(const std::string &id) {
    auto el = scenes[id];
    if(!el)
        return false;
    current = el;
    return true;
}

Engine::SceneCoordinator::~SceneCoordinator() {
    for (auto const& scene : scenes)
    {
        EN_CORE_INFO("Deleting scene with key " + scene.first);
        delete scene.second;
    }
}

Engine::Base_Scene *Engine::SceneCoordinator::getCurrentScene() {
    return current;
}

void Engine::SceneCoordinator::transitionTo(std::string id) {
    Base_Scene * newScene = scenes[id];
    if(!newScene)
        return;
    newScene->load();
    Base_Scene * old = current;
    current = newScene;
    old->unload();
}

Engine::Base_Scene * Engine::SceneCoordinator::getSceneById(const std::string id) {
    return scenes[id];
}
