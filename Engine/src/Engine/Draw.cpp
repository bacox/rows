//
// Created by bart on 5-1-19.
//

#include "Draw.h"

void Engine::Draw::Rectangle(int x1, int y1, int x2, int y2, ALLEGRO_COLOR color, int thickness) {
    al_draw_rectangle(x1, y1, x2, y2, color, thickness);
}

void Engine::Draw::Rectangle(Engine::Position p1, Engine::Position p2, ALLEGRO_COLOR color, int thickness) {
    Rectangle(p1.getX(), p1.getY(), p2.getX(), p2.getY(), color, thickness);
}

void Engine::Draw::Rectangle(Engine::Position p, Engine::Dimension d, ALLEGRO_COLOR color, int thickness) {
    Rectangle(p.getX(), p.getY(), p.getX() + d.getWidth(), p.getY() + d.getHeight(), color, thickness);
}

void Engine::Draw::RectangleFilled(int x1, int y1, int x2, int y2, ALLEGRO_COLOR color) {
    al_draw_filled_rectangle(x1, y1, x2, y2, color);
}

void Engine::Draw::RectangleFilled(Position p1, Position p2, ALLEGRO_COLOR color) {
    RectangleFilled(p1.getX(), p1.getY(), p2.getX(), p2.getY(), color);
}

void Engine::Draw::RectangleFilled(Position p, Dimension d, ALLEGRO_COLOR color) {
    Position p2(p.getX() + d.getWidth(), p.getY() + d.getHeight());
    RectangleFilled(p, p2, color);
}

void Engine::Draw::Line(int x1, int y1, int x2, int y2, ALLEGRO_COLOR color, int thickness) {
    al_draw_line(x1, y1, x2, y2, color, thickness);
}

void Engine::Draw::Line(Position p1, Position p2, ALLEGRO_COLOR color, int thickness) {
    Line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), color, thickness);
}

void Engine::Draw::Line(Position p, Dimension d, ALLEGRO_COLOR color, int thickness) {
    Position p2(p.getX() + (d.getWidth() / 2), p.getY() + (d.getHeight() / 2));
    Line(p, p2, color, thickness);
}

void Engine::Draw::Circle(float cx, float cy, float r, ALLEGRO_COLOR color, float thickness) {
    al_draw_circle(cx, cy, r, color, thickness);
}

void Engine::Draw::Circle(Position p, float r, ALLEGRO_COLOR color, float thickness) {
    Circle(p.getX(), p.getY(), r, color, thickness);
}
