//
// Created by bart on 18-1-19.
//

#include "Mouse.h"

void Engine::Mouse::updateMousePosition(int x, int y, int dx, int dy) {
    this->x = x;
    this->y = y;
    this->dx = dx;
    this->dy = dy;
}

void Engine::Mouse::changeElement(Engine::Base_Element *el) {
    //Unset last element
    if(element) {
        element->setHover(false);
    }
    element = el;
}
