//
// Created by bacox on 7-5-19.
//

#ifndef ROWS_SCENEMANAGER_H
#define ROWS_SCENEMANAGER_H


class SceneManager {
private:
    SceneManager();

public:
    static SceneManager& getInstance();
    SceneManager(SceneManager const&) = delete;
    void operator=(SceneManager const&) = delete;
};


#endif //ROWS_SCENEMANAGER_H
