//
// Created by bart on 13-1-19.
//

#include "Layer.h"

void Engine::Layer::addElement(Engine::Base_Element *el) {
    elements.push_back(el);
}

void Engine::Layer::draw() {

    // Draw all elements
    for(Base_Element * el : elements) {
        if(el) {
            if(!el->isHidden())
                el->draw();
        }
    }
}

Engine::Base_Element * Engine::Layer::hasElement(Engine::Position p) {
    for(Base_Element *el : elements) {
        if(el->hasPosition(p)) {
            if(el->isNested())
            {
                return el->getNestedPosition(p);
            }
            return el;
        }
    }
    return nullptr;
}

void Engine::Layer::destroyAllElements() {
    for(Base_Element * el : elements) {
        delete el;
    }
    elements.clear();
}
