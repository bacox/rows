//
// Created by bart on 6-1-19.
//

#include "Dimension.h"

Engine::Dimension::Dimension(int width, int height) : height(height), width(width) {}

int Engine::Dimension::getHeight() const {
    return height;
}

int Engine::Dimension::getWidth() const {
    return width;
}

void Engine::Dimension::resize(int w, int h) {
    width = w;
    height = h;
}

std::string Engine::Dimension::toString() {
    return "Dimension{width: "+ std::to_string(width) +", height:" + std::to_string(height) + "}";
}

Engine::Dimension::Dimension() {
    width = 0;
    height = 0;
}
