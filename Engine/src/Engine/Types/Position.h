//
// Created by bart on 6-1-19.
//

#ifndef ROWS_POSITION_H
#define ROWS_POSITION_H

#include <string>

namespace Engine{
        class Position {
        private:
            int x;
            int y;
        public:
            int getX() const;

            int getY() const;
            void move(int x, int y);

        public:
            std::string toString();
            Position(int x, int y);
            Position();
        };
}




#endif //ROWS_POSITION_H
