//
// Created by bart on 6-1-19.
//

#ifndef ROWS_DIMENSION_H
#define ROWS_DIMENSION_H
#pragma once
#include "Position.h"

namespace Engine {

    class Dimension {
    private:
        int height;
        int width;
    public:
        int getHeight() const;

        int getWidth() const;
        void resize(int w, int h);

        std::string toString();
        Dimension(int width, int height);
        Dimension();
    };
}


#endif //ROWS_DIMENSION_H
