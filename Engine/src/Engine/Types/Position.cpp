//
// Created by bart on 6-1-19.
//

#include "Position.h"

Engine::Position::Position(int x, int y) : x(x), y(y) {}
Engine::Position::Position() : x(0), y(0) {}

int Engine::Position::getX() const {
    return x;
}

int Engine::Position::getY() const {
    return y;
}

void Engine::Position::move(int x, int y) {
    this->x = x;
    this->y = y;
}

std::string Engine::Position::toString() {
    return "Point{x: "+ std::to_string(x) +", y:" + std::to_string(y) + "}";
}
